# Map Service Image Tiler
Joins image tiles retrieved from Mapbox or Thunderforest services to use for printed hobby and adventure maps.

You are legally required to include proper attribution:

https://docs.mapbox.com/help/getting-started/attribution/#when-do-you-have-to-provide-attribution

https://www.thunderforest.com/terms/

## Setup:
**Installation:** Download to the folder you want to run in.

**VENV**: A virtual environment using a Python 3.8 interpreter, or newer, should be setup for this project.

https://docs.python.org/3/tutorial/venv.html

#### Required packages:
* Requests
* Pillow

#### Access Tokens: 
**API keys are required.**

These should be setup as environment variables depending on the service being used.

**MAPBOX_ACCESS_TOKEN**: https://docs.mapbox.com/help/getting-started/access-tokens/

**THUNDERFOREST_ACCESS_TOKEN**: https://www.thunderforest.com/docs/apikeys/

## Usage
**map_maker.py** can be run on its own from the project's root folder:

`python map_maker.py` will use the `default.py` configuration file found in the `configs` folder.

`python map_maker.py -c custom_config` Will use `custom_config` in the `configs` folder specified without `.py`

Finished files will be saved to the `images` folder


### Config Files
These define the details for a project

`name` A filename prefix to help identify project files

`service` Defines if we are using `mapbox` or `thunderforest` services

`zoom_level` A number between 0 and 22. Fractional zoom levels will be rounded to two decimal places

`coordinates` Coordinates in `longitude,latitude` format for the northwestern corner of the area to cover. 
- Latitudes between -85.0511 and 85.0511

`styles` A list of dictionaries containing API style info.  `map_maker.py` will loop through each entry.

    styles = [
    {'id': 'satellite-v9', 'name': 'satellite-v9', 'username': 'mapbox'},
    {'id': 'outdoors-v11',   # style id used by API server
     'name': 'outdoors-v11', # name to use for style used in filename
     'username': 'mapbox'},  # username for API service if required
    ]

`tile_size` Pixel size of image to request.  See service notes 

`height`,`width` Height and width of our finished map in multiples of tile_size

`high_dpi` Default `False` returns a 300 dpi image.  Using `True` results in a 600 dpi image.

`geojson` URL encoded geojson.  See API specifications

`overlay` Empty string `''` or `f'geojson({geojson})/'`

`options` See API URL specifications, replaces `{bearing},{pitch}|{bbox}|{auto}` in URL

### Service Notes
**Mapbox:**

Static Images API is used if the configured tile size is larger than 512px
 - Maximum tile size is 1280px
 - Uses longitude and latitude coordinates
 - Has options like geojson overlays
 - Accurate starting at z11 for 1024px and z12 for 1280px
 - Configurations between 512px and 1024px should be avoided since these are an inefficient API usage cost
 - https://docs.mapbox.com/api/maps/static-images/

Static Tiles API is used if the configured tile size is 512
 - Only uses 512px tiles
 - Does not have additional options offered by the Static Images API
 - Accurate at any zoom level
 - Uses an x y z grid position
 - https://docs.mapbox.com/api/maps/static-tiles/

**Thunderforest:**

Currently, only Static Images are supported
 - Maximum tile size is 2560px
 - Uses longitude and latitude coordinates
 - Static Map API requests count as 10 Map Tile API requests
 
## Todo:
- add ThunderForest Map Tiles API support
- add route import
- add layers
- add masking
- add geotagged PDF output