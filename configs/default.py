name = 'default'  # used as a filename prefix
service = 'mapbox'  # API service to use
zoom_level = 15  # accurate starting at z11 for 1024px and z12 for 1280px
# Latitude boundaries are at 85˚ and -85˚
# Northwest corner of area to cover specified as Longitude,Latitude
coordinates = -68.79, -14.881
# map_maker.py will loop through each of the available styles
styles = [
    # {'id': 'satellite-v9', 'name': 'satellite-v9', 'username': 'mapbox'},
    {'id': 'outdoors-v11', 'name': 'outdoors-v11', 'username': 'mapbox'},
]
tile_size = 1024  # px size of image to request
# Height and Width of our finished map in multiples of tile_size
height = 3 * tile_size
width = 3 * tile_size
high_dpi = False  # Default `False` returns a 300 image.  Using `True` results in a 600 dpi image.
# Additional Mapbox customizations available through the Static Images API
geojson = ''  # URL encoded geojson.  See API specifications todo: implement file import for this
overlay = ''  # empty string '' or 'geojson({geojson})/' todo: make into function
options = ''  # See API specifications, replaces {bearing},{pitch}|{bbox}|{auto} in URL
