"""
Pixel perfect tiling of map service images for printed hobby and adventure maps.

You are legally required to include proper attribution.
https://docs.mapbox.com/help/getting-started/attribution/#when-do-you-have-to-provide-attribution
https://www.thunderforest.com/terms/
"""
import argparse
import io
import math
import os
from time import perf_counter

import requests
from PIL import Image

# Initialize parser
parser = argparse.ArgumentParser()
# Add arguments
parser.add_argument('-c', '--config',
                    help='Specify a configuration in the `configs` folder without `.py`')
# Only parse known arguments to avoid 'unknown-argument' error with pytest
args, unknown = parser.parse_known_args()

if args.config:  # use a configuration specified through the commandline
    print(f'Config File: configs/{args.config}')
    import importlib.util
    spec = importlib.util.spec_from_file_location(f'{args.config}',
                                                  f'{os.getcwd()}/configs/{args.config}.py')
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    config = module
else:  # use the default config if no configuration is specified
    from configs import default as config


class Location:
    """
    You are here. Tracks the longitude and latitude for our requests
    and relates them to an x y z grid position scaled for changes in tile px size.

    https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python
    """
    def __init__(self, conf=config):
        scale = {'mapbox': 512, 'thunderforest': 256}
        self.px_scale = conf.tile_size / scale[conf.service]
        self.zoom_level = conf.zoom_level
        self.grid_limit = (self.degrees_to_tile(180, -85.051128)[0])  # bottom right grid corner
        self.grid_x = self.degrees_to_tile(conf.coordinates[0], conf.coordinates[1])[0]
        self.grid_y = self.degrees_to_tile(conf.coordinates[0], conf.coordinates[1])[1]
        self.longitude, self.latitude = self.tile_to_degrees(self.grid_x, self.grid_y)

    def __str__(self):
        return f'Coordinates: {self.longitude}, {self.latitude}\n' \
               f'Grid Tile:   {self.grid_x}, {self.grid_y}, {self.zoom_level}, {self.grid_limit}'

    def degrees_to_tile(self, lng, lat):
        """
        Converts coordinates in degrees to an x y z grid tile position
        :param lng: longitude of the location we're converting to grid_x
        :param lat: latitude of the location we're converting to grid_y
        :return: x y z grid tile position
        """
        lat_rad = math.radians(lat)
        tiles = 2 ** self.zoom_level
        grid_x = (lng + 180) / 360 * tiles
        grid_y = (1 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2 * tiles
        return grid_x, grid_y

    def tile_to_degrees(self, grid_x, grid_y):
        """
        Converts an x y z grid position to longitude and latitude
        :param grid_x: grid_x position to convert to longitude
        :param grid_y: grid_y position to convert to latitude
        :return: Longitude and latitude of our current grid tile position
        """
        tiles = 2 ** self.zoom_level
        lng = grid_x / tiles * 360 - 180
        lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * grid_y / tiles)))
        lat = math.degrees(lat_rad)
        return round(lng, 8), round(lat, 8)

    def grid_position(self):
        """
        :return: x y grid position for our current longitude and latitude
        """
        return self.degrees_to_tile(self.longitude, self.latitude)

    def next_lng(self):
        """
        :return: next longitude to request
        """
        next_x = self.grid_x + self.px_scale
        if next_x > self.grid_limit:
            next_x -= self.grid_limit
        next_x_lng = self.tile_to_degrees(next_x, self.grid_y)[0]
        return next_x_lng

    def next_lat(self):
        """
        :return: next latitude to request
        """
        next_y = self.grid_y + self.px_scale
        next_y_lat = self.tile_to_degrees(self.grid_x, next_y)[1]
        return next_y_lat

    def update_lng(self, new_longitude):
        """
        Updates our longitude and sets the corresponding grid x position
        :param new_longitude: longitude to use
        """
        self.longitude = new_longitude
        self.grid_x = self.grid_position()[0]

    def update_lat(self, new_latitude):
        """
        Updates our latitude and sets the corresponding grid y position
        :param new_latitude: latitude to use
        """
        self.latitude = new_latitude
        self.grid_y = self.grid_position()[1]


class MapService:
    """
    Handles map service requests
    todo: Add exception handling for responses
    """
    def __init__(self, _location):
        self.location = _location

    def mapbox_styles(self, _style, conf=config, attribution='false', logo='false'):
        """
        Returns a URL for Mapbox API services determined by the configured tile size
        :param _style: index of a style from our configured styles
        :param conf: configuration to use
        :param attribution: whether to include an attribution watermark
        :param logo: whether to include a Mapbox logo
        :return: Mapbox URL
        """
        server = 'api.mapbox.com'
        api = 'styles'
        api_version = 'v1'
        username = conf.styles[_style]['username']
        style_id = conf.styles[_style]['id']
        token = os.environ.get('MAPBOX_ACCESS_TOKEN')
        tile_size = conf.tile_size
        dpi = ''
        url = ''
        if conf.high_dpi:
            dpi = '@2x'
        # Use Mapbox Static Images API if tiles are larger than 512px, or if there's an overlay
        if tile_size > 512 or conf.overlay != '':
            url = f'https://{server}/{api}/{api_version}/{username}' \
                  f'/{style_id}/static/{conf.overlay}{self.location.longitude}' \
                  f',{self.location.latitude},{self.location.zoom_level}' \
                  f'/{tile_size}x{tile_size}{dpi}?attribution={attribution}&logo={logo}' \
                  f'{conf.options}&access_token={token}'
        elif tile_size == 512:  # use Mapbox Static Tiles API if tiles are 512px
            url = f'https://{server}/{api}/{api_version}/{username}' \
                  f'/{style_id}/tiles/{conf.tile_size}/{self.location.zoom_level}' \
                  f'/{int(self.location.grid_x)}/{int(self.location.grid_y)}' \
                  f'{dpi}?attribution={attribution}&logo={logo}&access_token={token}'
        return url

    def thunder_forest(self, _style, conf=config, extension='png'):
        """
        Returns a URL for ThunderForest API services
        :param _style: index of a style from our configured styles
        :param conf: configuration to use
        :param extension: png or jpg90 image type extensions
        :return: ThunderForest URL
        """
        server = 'tile.thunderforest.com'
        api = 'static'
        style_id = conf.styles[_style]['id']
        tile_size = conf.tile_size
        token = os.environ.get('THUNDERFOREST_ACCESS_TOKEN')
        dpi = ''
        if conf.high_dpi:
            dpi = '@2x'
        return f'https://{server}/{api}/{style_id}' \
               f'/{self.location.longitude},{self.location.latitude},{self.location.zoom_level}' \
               f'/{tile_size}x{tile_size}{dpi}.{extension}?apikey={token}'

    def url(self, _style, conf=config):
        """
        Returns a URL based on the configured map service
        :param _style: index of a style from our configured styles
        :param conf: configuration to use
        :return: map service URL
        """
        url = None
        if conf.service == 'mapbox':
            url = self.mapbox_styles(_style, conf)
        elif conf.service == 'thunderforest':
            url = self.thunder_forest(_style, conf)
        return url

    @staticmethod
    def get(url):
        """
        Requests our map service URL and returns (response content, status code)
        :param url: map service URL
        :return: content and status code from our request as (BytesIO buffer, status_code)
        """
        response = requests.get(url)
        return io.BytesIO(response.content), response.status_code


class TiledImage:
    """
    An image instance and methods to generate our finished map file.
    """
    def __init__(self, conf=config):
        self.location = Location(conf)
        self.width, self.height = conf.width, conf.height
        self.tile_size = conf.tile_size
        self.image = None
        self.style = {}
        self.path = ''
        if conf.high_dpi:
            self.width *= 2
            self.height *= 2

    def new(self):
        """
        Creates a new empty image using the configured width and height
        """
        self.image = Image.new('RGBA', (self.width, self.height))

    def paste(self, service_img, image_x, image_y):
        """
        Pastes the image received from our map service
        :param service_img: image received from our request
        :param image_x: horizontal offset for our pasted image
        :param image_y: vertical offset for our pasted image
        """
        self.image.paste(service_img, (image_x, image_y))

    def make(self, _style, conf=config):
        """
        Makes our finished image for the current style
        :param _style: index of a style from our configured styles
        :param conf: configuration to use
        :return: saved image filepath
        """
        location = self.location
        service = MapService(location)
        x_tiles = int(self.width / self.tile_size)
        y_tiles = int(self.height / self.tile_size)
        img_x = 0

        self.new()
        for _ in range(0, x_tiles):
            for y_tile in range(0, y_tiles):
                print(f'{location}\n')
                url = service.url(_style, conf)
                response = service.get(url)
                img = Image.open(response[0])
                img_y = conf.tile_size * y_tile
                if conf.high_dpi:
                    img_y *= 2

                self.paste(img, img_x, img_y)
                location.update_lat(location.next_lat())

            img_x += conf.tile_size
            if conf.high_dpi:
                img_x += conf.tile_size
            location.update_lat(conf.coordinates[1])
            location.update_lng(location.next_lng())

        self.save(_style)
        self.close()
        print(f'Image file: {self.path}')
        return self.path

    def save(self, _style, conf=config, img_dir='images', extension='png'):
        """
        Saves the final image and returns the file path
        :param _style: index of the configured style to use
        :param extension: image type extension to use
        :param conf: configuration to use
        :param img_dir: filepath of folder to save into
        :return: saved image filepath
        """
        dpi = 300
        high_dpi = ''
        if conf.high_dpi:
            dpi *= 2
            high_dpi = '2x'
        self.path = f'{img_dir}/{conf.name}_{conf.styles[_style]["name"]}' \
                    f'_z{self.location.zoom_level}_{conf.tile_size}px{high_dpi}.{extension}'
        self.image.save(self.path, dpi=(dpi, dpi))
        return self.path

    def close(self):
        """
        Closes the image
        """
        self.image.close()


if __name__ == '__main__':
    start = perf_counter()
    for style in range(0, len(config.styles)):
        image = TiledImage(config)
        image.make(style)
    end = perf_counter()
    print(f'Completed in: {end - start} seconds')
