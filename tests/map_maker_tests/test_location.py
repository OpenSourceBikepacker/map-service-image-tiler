"""
Test that our Location instance correctly updates and converts degree and tile positions
"""
import pytest

from map_maker import Location
from tests.configs import mapbox_z10_test_config as mapbox_z10
from tests.configs import thunderforest_z11_high_dpi_test_config as thunderforest_z11


@pytest.fixture(name='location')
def fixture_location(conf):
    """
    :return: a new Location instance
    """
    return Location(conf=conf)


@pytest.mark.parametrize("conf", [mapbox_z10, thunderforest_z11])
def test_config(location, conf):
    """
     Assert the Location created by the location fixture is using our test configuration
    :param location: Location instance provided by the location test fixture
    :param conf: Test configuration.  Also used by the location fixture
     """
    high_dpi = ''
    if conf.high_dpi:
        high_dpi = '_high_dpi'

    assert conf.zoom_level == location.zoom_level
    assert conf.name == f'{conf.service}_z{location.zoom_level}{high_dpi}_test'


@pytest.mark.parametrize('conf, expected', [
    (mapbox_z10, 1024),
    (thunderforest_z11, 2048)
])
def test_grid_limit(location, conf, expected):  # pylint: disable=unused-argument
    """
    Assert degrees_to_tile is returning correct values by checking the results of grid_limit
    :param location: Location instance provided by the location test fixture
    :param conf: Test configuration.  Also used by the location fixture
    :param expected: Expected results to validate against
    """
    assert location.grid_limit == expected


@pytest.mark.parametrize('conf, expected', [
    (mapbox_z10, (-111.69, 35.35)),
    (thunderforest_z11, (-68.117, -16.39))
])
def test_tile_to_degrees(location, conf, expected):  # pylint: disable=unused-argument
    """
    Assert tile_to_degrees returns correct longitude and latitude values
    :param location: Location instance provided by the location test fixture
    :param conf: Test configuration used by the location fixture
    :param expected: Results to validate against
    """
    grid_x = location.grid_x
    grid_y = location.grid_y
    results = location.tile_to_degrees(grid_x, grid_y)
    assert (results[0], results[1]) == expected


@pytest.mark.parametrize('conf, expected', [
    (mapbox_z10, (-110.986875, 34.77447695)),
    (thunderforest_z11, (-67.94121875, -16.55856485))
])
def test_update_locations(location, conf, expected):  # pylint: disable=unused-argument
    """
    Assert longitude and latitude are updated correctly
    :param location: Location instance provided by the location test fixture
    :param conf: Test configuration used by the location fixture
    :param expected: Results to validate against
    """
    assert (location.next_lng(), location.next_lat()) == expected
    location.update_lng(location.next_lng())
    location.update_lat(location.next_lat())
    assert (location.longitude, location.latitude) == expected
