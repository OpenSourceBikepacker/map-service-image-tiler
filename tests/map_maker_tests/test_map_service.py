"""
Test that a MapService instance correctly formats URLs
Test that the response from the map server is an image
Test that the image is the correct size
"""
import os

import magic
import pytest
from PIL import Image

from map_maker import Location, MapService
from tests.configs import mapbox_z10_test_config as mapbox_z10
from tests.configs import mapbox_z15_test_config as mapbox_z15
from tests.configs import thunderforest_z11_high_dpi_test_config as thunderforest_z11


@pytest.fixture(name='location')
def fixture_location(conf):
    """
    Provides a Location instance for our tests
    :param conf: configuration to use
    :return: a new location instance
    """
    return Location(conf=conf)


@pytest.fixture(name='map_service')
def fixture_map_service(location):
    """
    Provides a new MapService instance for our tests
    :param location: Location instance provided by the location pytest fixture
    :return: a new MapService instance
    """
    return MapService(location)


@pytest.mark.parametrize("conf", [mapbox_z10, mapbox_z15, thunderforest_z11])
def test_url(location, map_service, conf):
    """Assert URLs are correctly formatted"""
    style = 0
    username = conf.styles[style]['username']
    style_id = conf.styles[style]['id']
    lng = location.longitude
    lat = round(location.latitude, 8)
    tile_size = conf.tile_size
    url_middle = ''
    expected = ''
    dpi = ''
    if conf.high_dpi:
        dpi = '@2x'

    if conf.service == 'mapbox':
        token = os.environ.get('MAPBOX_ACCESS_TOKEN')
        if tile_size > 512:
            url_middle = f'static/{lng},{lat},{conf.zoom_level}/{tile_size}x{tile_size}'
        elif tile_size == 512:
            url_middle = f'tiles/{conf.tile_size}/{location.zoom_level}' \
                         f'/{int(location.grid_x)}/{int(location.grid_y)}'

        expected = f'https://api.mapbox.com/styles/v1/{username}/{style_id}' \
                   f'/{url_middle}' \
                   f'{dpi}?attribution=false&logo=false&access_token={token}'
    elif conf.service == 'thunderforest':
        token = os.environ.get('THUNDERFOREST_ACCESS_TOKEN')
        expected = f'https://tile.thunderforest.com/static/{style_id}' \
                   f'/{lng},{lat},{conf.zoom_level}' \
                   f'/{tile_size}x{tile_size}{dpi}.png' \
                   f'?apikey={token}'
    url = map_service.url(style, conf)
    assert url == expected


@pytest.mark.parametrize("conf", [mapbox_z10, mapbox_z15, thunderforest_z11])
def test_get(map_service, conf):
    """Assert we receive an image response and that it is the correct size"""
    style = 0
    url = map_service.url(style, conf)
    response = map_service.get(url)
    size = conf.tile_size
    if conf.high_dpi:
        size *= 2
    status_code = response[1]
    assert status_code == 200  # good for debugging
    mime = magic.from_buffer(response[0].read(1024), mime=True)
    assert mime.startswith('image')
    assert Image.open(response[0]).size[0] == size
