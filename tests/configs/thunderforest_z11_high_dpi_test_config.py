name = 'thunderforest_z11_high_dpi_test'  # used as a final file prefix
service = 'thunderforest'
zoom_level = 11
# Northwest corner of area to cover specified as Longitude,Latitude
coordinates = -68.117, -16.39

styles = [
    {'id': 'landscape', 'name': 'test-thunderforest', 'username': ''},
]

tile_size = 256
height = 3 * tile_size
width = 3 * tile_size
high_dpi = True  # Results in a 600 dpi file.  Default False

# Additional Mapbox options available through the Static Images API
# See API doc for details
# todo: add handling geojson as file
geojson = ''
overlay = ''  # empty string '' or 'geojson({geojson})/'
options = ''  # replaces {bearing},{pitch}|{bbox}|{auto} in URL
