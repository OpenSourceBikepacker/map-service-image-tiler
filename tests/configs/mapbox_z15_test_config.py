name = 'mapbox_z15_test'  # used as a final file prefix
service = 'mapbox'
zoom_level = 15
# Northwest corner of area to cover specified as Longitude,Latitude
coordinates = -111.69, 35.35

styles = [
    {'id': 'outdoors-v11', 'name': 'test-outdoors-v11', 'username': 'mapbox'},
]

tile_size = 512
height = 3 * tile_size
width = 3 * tile_size
high_dpi = False  # Results in a 600 dpi file.  Default False

# Additional Mapbox customizations available through the Static Images API
# See API doc for details
geojson = ''
overlay = ''  # empty string '' or 'geojson({geojson})/'
options = ''

# Additional ThunderForest options
thunderforest_scale = 1
