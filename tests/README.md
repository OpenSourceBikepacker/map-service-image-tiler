# Map Service Image Tiler Tests
Pytest scripts to validate things are working as expected.

## Setup
**Requires additional packages for testing**

#### Required Packages
* Pytest
* Python-magic

## Usage
Running `pytest` in the project's root folder or from the `tests` folder will execute all tests.

Test scripts are sorted into folders named after the module they are testing and are named after the `ClassName` they cover. 

e.g. `tests/map_maker_tests/test_location.py` has validations against functionality in the `Location` class of `map_maker.py`

### Test Configs
Test scripts use configurations tailored to produce expected results.

Configurations and results are passed through using pytest decorators like the following example:

    @pytest.mark.parametrize('conf, expected', [
        (mapbox_z10, 1024),
        (thunderforest_z11, 2048)
    ])

It is recommended to name test configuration files using the `name` value from the configuration followed by `test_config`.

e.g. `thunderforest_z11_high_dpi_test_config.py` should have the following values:

    name = thunderforest_z11_high_dpi_test
    service = `thunderforest`
    zoom_level = 11
    high_dpi = True

`name` is tested to match the following scheme `{service}_z{zoom_level}{high_dpi}_test` when we validate
 a Location instance has received the test config.

`service` and `zoom_level` should match the values used in the file.

`high_dpi` is a True False bool but is translated to 'high_dpi' for some tests.

`coordinates` , `tile_size` these are set to specific values, so we can anticipate expected test results.


